#include <stdio.h> // Used for printf() statements
#include <wiringPi.h> // Include WiringPi library!
#include <lcd.h> // Include LCD display

#define LCD_RS = 11  // pin 23
#define LCD_E = 5  // pin 29
#define LCD_D4 = 6   // pin 31
#define LCD_D5 = 13  // pin 33
#define LCD_D6 = 19  // pin 35
#define LCD_D7 = 26  // pin 37

const int redLED = 17; // redLED - pin 11 for alarm on
const int yellowLED = 27; // yellowLED - pin 13 for alarm armed
const int greenLED = 22; // greenLED - pin 15 for alarm off
const int button1 = 18;  // button1 - pin 12
const int button2 = 23;  // button2 - pin 16
const int button3 = 24;   // button3 - pin 18
const int button4 = 25;  // button4 - pin 22



void AlarmOff(){
        digitalWrite (redLED, LOW);
        digitalWrite (yellowLED, LOW);
        digitalWrite (greenLED, HIGH);
        printf("Alarm off\n");
}
 
void AlarmOn(){
        digitalWrite (redLED, HIGH);
        digitalWrite (yellowLED, LOW);
        digitalWrite (greenLED, LOW);
        printf("Alarm On\n");
}

void AlarmArmed(){
        digitalWrite (redLED, LOW);
        digitalWrite (yellowLED, HIGH);
        digitalWrite (greenLED, LOW);
        printf("System Armed\n");
}

int main(){

        // Setup:
        wiringPiSetupGpio();
        wiringPiSetup();
        // lcdInit (int rows, int cols, int bits, int rs, int strb, int d0, int d1, int d2, int d3, int d5, int d6, int d7) Format for LCD library
        int lcdDisplay;
        lcdDisplay = lcdInit (2, 16, 4, LCD_RS, LCD_E, LCD_D4, LCD_D5, LCD_D6, LCD_D7, 0, 0, 0, 0);

        pinMode(redLED, OUTPUT); // Set redLED as output
        pinMode(yellowLED, OUTPUT); // Set yellowLED as output
        pinMode(greenLED, OUTPUT); // Set greenLED as output
        pinMode(button1, INPUT); // Set button1 as INPUT
        pinMode(button2, INPUT); // Set button2 as INPUT
        pinMode(button3, INPUT); // Set button3 as INPUT
        pinMode(button4, INPUT); // Set button4 as INPUT

        lcdPuts (lcdDisplay, "Security System");

}




